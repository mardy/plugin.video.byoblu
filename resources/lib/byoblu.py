# -*- coding: utf-8 -*-

import xbmc
import xbmcaddon
import xbmcgui
import xbmcplugin

import re
import requests
from urllib.parse import urlencode, parse_qsl


ADDON_ID = 'plugin.video.byoblu'


class TheoPlayer(object):
    def __init__(self):
        pass

    def get_video_url(self, content):
        m = re.search(r'"(http.*([^"]*)/playlist.m3u8)"', content)
        if m:
            print('Matches: {}'.format(m.group(1)))
            return m.group(1)
        else:
            return None

class Byoblu(object):
    base_url = 'https://byoblu.com/'

    def __init__(self, args):
        self.log('__init__, args = ' + str(args))
        self.plugin_url = args[0]
        self.addon_handle = int(args[1])
        self.args = dict(parse_qsl(args[2][1:]))
        print('Arguments: {}'.format(self.args))
        self.addon = xbmcaddon.Addon()
        xbmcplugin.setContent(self.addon_handle, 'videos')

    def log(self, msg, level=xbmc.LOGDEBUG):
        if level == xbmc.LOGERROR: msg += ' ,' + traceback.format_exc()
        xbmc.log(ADDON_ID + ' - ' + msg, level)

    def build_url(self, **kwargs):
        return '{0}?{1}'.format(self.plugin_url, urlencode(kwargs))

    def get_all_channels(self):
        print('Ciao')
        item = xbmcgui.ListItem(label='Diretta TV')
        # item.setArt({'thumb': icon, 'icon': icon, 'fanart':icon})
        item.setInfo('video', {})
        item.setProperty('IsPlayable', 'true')
        url = self.build_url(mode='play', channel_url='diretta-tv/')
        xbmcplugin.addDirectoryItem(handle=self.addon_handle, url=url, listitem=item)

        xbmcplugin.endOfDirectory(self.addon_handle)

    def playVideo(self, channel_url):
        self.log("Asked to play video at {}".format(channel_url))
        r = requests.get(self.base_url + channel_url)
        print(r.status_code)
        print(r.content)
        parser = TheoPlayer()
        video_url = parser.get_video_url(r.text)
        print('Got video URL: {}'.format(video_url))
        name = 'Diretta TV'
        li = xbmcgui.ListItem(name, path=video_url)
        if 'm3u8' in video_url.lower():
            li.setProperty('inputstreamaddon','inputstream.adaptive')
            li.setProperty('inputstream.adaptive.manifest_type','hls')
        xbmcplugin.setResolvedUrl(self.addon_handle, True, li)
        

    def run(self):
        print('Run with args {}'.format(self.args))
        mode = self.args.get('mode', None)
        if mode == 'play':
            self.playVideo(self.args['channel_url'])
        else:
            self.get_all_channels()
